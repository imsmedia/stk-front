'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');
var autoprefixer = require('gulp-autoprefixer');
var spritesmith = require('gulp.spritesmith');

gulp.task('default', function() {
  console.log('Start gulp...');
});
gulp.task('refresh', function() {
  livereload.reload();
})
gulp.task('sass', () => {
  return gulp.src('./scss/*.scss')
    .pipe(sass({
        includePaths: [
          require('node-normalize-scss').includePaths,
          require('node-bourbon').includePaths
        ]
      })
      .on('error', sass.logError))
    .pipe(gulp.dest('./css'))
    .pipe(livereload());
});
gulp.task('watch', () => {
  livereload({start: true});
  gulp.watch('./scss/*.scss', ['sass']);
  gulp.watch(['./*.html', './js/*.js'], ['refresh']);
  gulp.watch('./css/*.css', ['autoprefixer']);
});
gulp.task('autoprefixer', () => {
  return gulp.src('./css/*.css')
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('./css'));
});
gulp.task('sprite', function() {
  var spriteData =
    gulp.src('./img/sprite/*.*')
    .pipe(spritesmith({
      imgName: 'sprite.png',
      cssName: 'sprite.css',
      imgPath: '/img/sprite.png'
    }));
  spriteData.img.pipe(gulp.dest('./img/'));
  spriteData.css.pipe(gulp.dest('./scss/'));
});
